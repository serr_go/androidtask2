package javacourse.kosyuksergey.androidtask2.mvp;

public class Presenter implements MainMVP.presenter{
    private final MainMVP.view view;

    public Presenter(MainMVP.view view) {
        this.view = view;
    }

    @Override
    public void addTaskClicked() {
        view.openNewTaskActivity();
    }
}
