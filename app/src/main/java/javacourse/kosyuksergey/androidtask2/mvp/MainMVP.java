package javacourse.kosyuksergey.androidtask2.mvp;

public interface MainMVP {
    interface view {
        void openNewTaskActivity();
    }
    interface presenter {
        void addTaskClicked();
    }
}
