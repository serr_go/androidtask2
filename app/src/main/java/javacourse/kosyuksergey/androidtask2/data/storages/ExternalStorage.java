package javacourse.kosyuksergey.androidtask2.data.storages;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.content.ContextCompat;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

import javacourse.kosyuksergey.androidtask2.R;
import javacourse.kosyuksergey.androidtask2.tab_fragments.model.Task;

public class ExternalStorage implements Storage {
    //public static ExternalStorage storage = new ExternalStorage();
    private static File externalFile;
    private ArrayList<Task> taskList;
    private Context context;
    private String path;

   /* private ExternalStorage() {
        taskList = new ArrayList<>();
    }

    public static Storage getInstance(Context context, String path) {
        storage.context = context;
        externalFile = new File(Environment.getExternalStorageDirectory(), path);
        return storage;
    }*/

    public ExternalStorage(Context context, String path) {
        taskList = new ArrayList<>();
        this.context = context;
        externalFile = new File(Environment.getExternalStorageDirectory(), path);
        this.path = path;
    }

    private boolean isExternalWritable() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isExternalReadable() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                || Environment.MEDIA_MOUNTED_READ_ONLY.equals(Environment.getExternalStorageState())) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkPermission(String permission) {
        int check = ContextCompat.checkSelfPermission(context, permission);
        return (check == PackageManager.PERMISSION_GRANTED);
    }

    private void saveData() {
        if (isExternalWritable() && checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Gson gson = new Gson();
            String jsonTaskList = gson.toJson(taskList);

            FileOutputStream fileOutputStream = null;

            try {
                fileOutputStream = new FileOutputStream(externalFile);
                fileOutputStream.write(jsonTaskList.getBytes());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void loadData() {
        if (isExternalReadable() && checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Gson gson = new Gson();
            FileInputStream internalFileInputStream = null;
            String data;

            try {
                internalFileInputStream = new FileInputStream(externalFile);
                InputStreamReader inputStreamReader = new InputStreamReader(internalFileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder stringBuilder = new StringBuilder();

                while ((data = bufferedReader.readLine()) != null) {
                    stringBuilder.append(data);
                }

                Type type = new TypeToken<ArrayList<Task>>() {
                }.getType();
                taskList = gson.fromJson(stringBuilder.toString(), type);
                if (taskList == null) {
                    taskList = new ArrayList<>();
                }
            } catch (FileNotFoundException e) {
                saveData();
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (internalFileInputStream != null) {
                    try {
                        internalFileInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void addToStorage(Task task) {
        loadData();
        taskList.add(task);
        saveData();
    }

    @Override
    public Task getFromStorage(int position) {
        return null;
    }

    @Override
    public ArrayList<Task> getStorage() {
        loadData();
        return taskList;
    }

    @Override
    public void removeFromStorage(int position) {
        loadData();
        taskList.remove(position);
        saveData();
    }
}
