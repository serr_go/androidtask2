package javacourse.kosyuksergey.androidtask2.data;

import android.content.Context;

import javacourse.kosyuksergey.androidtask2.R;
import javacourse.kosyuksergey.androidtask2.data.storages.DatabaseHelper;
import javacourse.kosyuksergey.androidtask2.data.storages.DatabaseStorage;
import javacourse.kosyuksergey.androidtask2.data.storages.ExternalStorage;
import javacourse.kosyuksergey.androidtask2.data.storages.InternalStorage;
import javacourse.kosyuksergey.androidtask2.data.storages.MemoryStorage;
import javacourse.kosyuksergey.androidtask2.data.storages.SharedPreferencesStorage;
import javacourse.kosyuksergey.androidtask2.data.storages.Storage;

public class StorageFactory {
    public Storage getStorage(StorageType type, Context context) {
        switch (type) {
            case MEMORY:
                return MemoryStorage.getInstance();
            case DATABASE:
                return new DatabaseStorage(context,"all_tasks");
            case EXTERNAL:
                return new ExternalStorage(
                        context,
                        context.getResources().getString(R.string.external_path_all));
            case INTERNAL:
                return new InternalStorage(
                        context,
                        context.getResources().getString(R.string.internal_path_all));
            case SHARED_PREFERENCES:
                return new SharedPreferencesStorage(
                        context,
                        context.getResources().getString(R.string.shared_pref_all_list_key));
            default:
                return null;
        }
    }

    public Storage getFeatureStorage(StorageType type, Context context) {
        switch (type) {
            case MEMORY:
                return MemoryStorage.getInstance();
            case DATABASE:
                return new DatabaseStorage(context,"feature_tasks");
            case EXTERNAL:
                return new ExternalStorage(
                        context,
                        context.getResources().getString(R.string.external_path_feature));
            case INTERNAL:
                return new InternalStorage(
                        context,
                        context.getResources().getString(R.string.internal_path_external_path_feature));
            case SHARED_PREFERENCES:
                return new SharedPreferencesStorage(
                        context,
                        context.getResources().getString(R.string.shared_pref_feature_list_key));
            default:
                return null;
        }
    }
}
