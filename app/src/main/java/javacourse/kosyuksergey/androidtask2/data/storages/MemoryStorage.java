package javacourse.kosyuksergey.androidtask2.data.storages;

import java.util.ArrayList;

import javacourse.kosyuksergey.androidtask2.tab_fragments.model.Task;

public class MemoryStorage implements Storage {
    private ArrayList<Task> taskList;
    static MemoryStorage storage = new MemoryStorage();

    private MemoryStorage() {
        taskList = new ArrayList<>();
    }

    public static Storage getInstance() {
        return storage;
    }

    @Override
    public void addToStorage(Task task) {
        taskList.add(task);
    }

    @Override
    public Task getFromStorage(int position) {
        return taskList.get(position);
    }

    @Override
    public ArrayList<Task> getStorage() {
        return taskList;
    }

    @Override
    public void removeFromStorage(int position) {
        taskList.remove(position);
    }
}
