package javacourse.kosyuksergey.androidtask2.data.storages;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

import javacourse.kosyuksergey.androidtask2.tab_fragments.model.Task;

public class InternalStorage implements Storage {
    //public static InternalStorage storage = new InternalStorage();
    private ArrayList<Task> taskList;
    private Context context;
    private String path;

    /*private InternalStorage() {
        taskList = new ArrayList<>();
    }

    public static Storage getInstance(Context context, String path) {
        storage.context = context;
        storage.path = path;
        return storage;
    }*/

    public InternalStorage(Context context, String path) {
        this.context = context;
        this.path = path;
    }

    private void saveData() {
        Gson gson = new Gson();
        String jsonTaskList = gson.toJson(taskList);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = context.openFileOutput(path, Context.MODE_PRIVATE);
            fileOutputStream.write(jsonTaskList.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void loadData() {
        Gson gson = new Gson();
        FileInputStream internalFileInputStream = null;
        String data;

        try {
            internalFileInputStream = context.openFileInput(path);
            InputStreamReader inputStreamReader = new InputStreamReader(internalFileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();

            while ((data = bufferedReader.readLine()) != null) {
                stringBuilder.append(data);
            }

            Type type = new TypeToken<ArrayList<Task>>() {
            }.getType();
            taskList = gson.fromJson(stringBuilder.toString(), type);
            if (taskList == null) {
                taskList = new ArrayList<>();
            }
        } catch (FileNotFoundException e) {
            saveData();
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (internalFileInputStream != null) {
                try {
                    internalFileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void addToStorage(Task task) {
        loadData();
        taskList.add(task);
        saveData();
    }

    @Override
    public Task getFromStorage(int position) {
        return null;
    }

    @Override
    public ArrayList<Task> getStorage() {
        loadData();
        return taskList;
    }

    @Override
    public void removeFromStorage(int position) {
        loadData();
        taskList.remove(position);
        saveData();
    }
}
