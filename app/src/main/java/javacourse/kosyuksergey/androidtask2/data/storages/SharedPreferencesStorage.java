package javacourse.kosyuksergey.androidtask2.data.storages;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;

import javacourse.kosyuksergey.androidtask2.tab_fragments.model.Task;

public class SharedPreferencesStorage implements Storage {
    //public static SharedPreferencesStorage storage = new SharedPreferencesStorage();

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    private String path;

    private ArrayList<Task> taskList;

    /*private SharedPreferencesStorage() {
        taskList = new ArrayList<>();
    }

    public static Storage getInstance(Context context, String path) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
        storage.path = path;
        return storage;
    }*/

    public SharedPreferencesStorage(Context context, String path) {
        taskList = new ArrayList<>();
        this.path = path;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
    }

    private void saveData() {
        Gson gson = new Gson();
        String jsonTaskList = gson.toJson(taskList);
        editor.putString(path, jsonTaskList);
        editor.commit();
    }

    private void loadData() {
        Gson gson = new Gson();
        String jsonTaskList = sharedPreferences.getString(path, null);
        Type type = new TypeToken<ArrayList<Task>>() {}.getType();
        taskList = gson.fromJson(jsonTaskList, type);
        if (taskList == null) {
            taskList = new ArrayList<>();
        }
    }

    @Override
    public void addToStorage(Task task) {
        loadData();
        taskList.add(task);
        saveData();
    }

    @Override
    public Task getFromStorage(int position) {
        return null;
    }

    @Override
    public ArrayList<Task> getStorage() {
        loadData();
        return taskList;
    }

    @Override
    public void removeFromStorage(int position) {
        loadData();
        taskList.remove(position);
        saveData();
    }
}
