package javacourse.kosyuksergey.androidtask2.data.storages;

import java.util.ArrayList;

import javacourse.kosyuksergey.androidtask2.tab_fragments.model.Task;

public interface Storage {
    void addToStorage(Task task);
    Task getFromStorage(int position);
    ArrayList<Task> getStorage();
    void removeFromStorage(int position);
}
