package javacourse.kosyuksergey.androidtask2.data;

public enum StorageType {
    MEMORY,
    SHARED_PREFERENCES,
    EXTERNAL,
    INTERNAL,
    DATABASE,;

    @Override
    public String toString() {
        switch(this) {
            case MEMORY: return "MEMORY";
            case SHARED_PREFERENCES: return "SHARED_PREFERENCES";
            case EXTERNAL: return "EXTERNAL";
            case INTERNAL: return "INTERNAL";
            case DATABASE: return "DATABASE";
            default: throw new IllegalArgumentException();
        }
    }
}
