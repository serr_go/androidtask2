package javacourse.kosyuksergey.androidtask2.data.storages;

import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import javacourse.kosyuksergey.androidtask2.tab_fragments.model.Task;

public class DatabaseStorage implements Storage {
    ArrayList<Task> taskList;
    DatabaseHelper databaseHelper;
    String path;

    public DatabaseStorage(Context context, String path) {
        taskList = new ArrayList<>();
        databaseHelper = new DatabaseHelper(context);
        this.path = path;
    }

    private void saveData() {
        Gson gson = new Gson();
        String jsonTaskList = gson.toJson(taskList);
        boolean insertData = databaseHelper.addData(jsonTaskList, path);
    }


    private void loadData() {
        Gson gson = new Gson();
        Cursor data = databaseHelper.getData(path);

        //
        int col = 0;
        if (path == "feature_tasks") {
            col = 1;
        }
        //

        try {
            //data.moveToNext();

            ///////////////////////////////////////////////////
            String jsonTaskList = data.getString(col);
            ///////////////////////////////////////////////////
            Type type = new TypeToken<ArrayList<Task>>() {}.getType();
            taskList = gson.fromJson(jsonTaskList, type);

        } catch (CursorIndexOutOfBoundsException e) {
            System.out.println(data.toString());
        }
        if (taskList == null) {
            taskList = new ArrayList<>();
        }
    }

    @Override
    public void addToStorage(Task task) {
        //loadData();
        taskList.add(task);
        saveData();
    }

    @Override
    public Task getFromStorage(int position) {
        return null;
    }

    @Override
    public ArrayList<Task> getStorage() {
        loadData();
        return taskList;
    }

    @Override
    public void removeFromStorage(int position) {
        loadData();
        taskList.remove(position);
        saveData();
    }
}
