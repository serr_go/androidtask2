package javacourse.kosyuksergey.androidtask2.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;

import javacourse.kosyuksergey.androidtask2.NewTaskActivity;
import javacourse.kosyuksergey.androidtask2.R;
import javacourse.kosyuksergey.androidtask2.data.StorageFactory;
import javacourse.kosyuksergey.androidtask2.data.StorageType;
import javacourse.kosyuksergey.androidtask2.data.storages.Storage;
import javacourse.kosyuksergey.androidtask2.loaders.FeatureLoader;
import javacourse.kosyuksergey.androidtask2.tab_fragments.model.Task;

public class TaskListAdapter extends ArrayAdapter<Task> {

    private Context mContext;
    private int mRecourse;
    private ArrayList<Task> taskList;

    public static class ViewHolder {
        TextView title;
        TextView description;
    }

    public TaskListAdapter(Context context, int resource, ArrayList<Task> tasks) {
        super(context, resource, tasks);
        mContext = context;
        mRecourse = resource;
        taskList = tasks;
    }

    @Nullable
    @Override
    public Task getItem(int position) {
        return taskList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return taskList.size();
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String title = getItem(position).getTitle();
        String description = getItem(position).getDescription();

        final Task task = new Task(title, description);

        final ViewHolder holder;
        final View popupButton;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mRecourse, parent, false);

            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.description = (TextView) convertView.findViewById(R.id.description);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(task.getTitle());
        holder.description.setText(task.getDescription());

        popupButton = convertView.findViewById(R.id.imageview_task_menu);
        popupButton.setTag(getItem(position));
        popupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(mContext, popupButton);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.list_item_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        StorageFactory storageFactory = new StorageFactory();
                        SharedPreferences sharedPreferences =
                                PreferenceManager.getDefaultSharedPreferences(getContext());
                        String storageType = sharedPreferences.getString(
                                getContext().getResources().getString(R.string.shared_pref_storage_type_key),
                                "");

                        Storage storage;
                        Intent intent;

                        switch (item.getItemId()) {
                            case R.id.edit_task:
                                intent = new Intent(getContext(), NewTaskActivity.class);
                                intent.putExtra("Edit Item", taskList.get(position));

                                storage =
                                        storageFactory.getStorage(
                                                StorageType.valueOf(storageType),
                                                getContext());

                                storage.removeFromStorage(position);

                                getContext().startActivity(intent);

                                swapData(storage.getStorage());
                                return true;
                            case R.id.delete_task:
                                storage =
                                        storageFactory.getStorage(
                                                StorageType.valueOf(storageType),
                                                getContext());

                                storage.removeFromStorage(position);
                                swapData(storage.getStorage());
                                return true;
                            case R.id.feature_task:
                                storage = storageFactory.getFeatureStorage(
                                        StorageType.valueOf(storageType),
                                        getContext());
                                storage.addToStorage(task);

                                intent = new Intent(FeatureLoader.ACTION);
                                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
                                return true;
                            default:
                                return false;
                        }
                    }
                });

                popup.show(); //showing popup menu
            }
        });

        return convertView;
    }

    public void swapData(ArrayList<Task> data) {
        taskList.clear();
        if (data == null) {
            taskList = new ArrayList<>();
        } else {
            taskList.addAll(data);
        }
        notifyDataSetChanged();
    }
}
