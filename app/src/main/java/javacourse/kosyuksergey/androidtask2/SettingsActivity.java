package javacourse.kosyuksergey.androidtask2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import javacourse.kosyuksergey.androidtask2.data.StorageType;

import static javacourse.kosyuksergey.androidtask2.data.StorageType.*;


public class SettingsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.settings_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        //
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        loadStorageType(sharedPreferences);


        RadioGroup rg = (RadioGroup) findViewById(R.id.settings_radio_group);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.sharedPref:
                        editor.putString(
                                getResources().getString(R.string.shared_pref_storage_type_key),
                                SHARED_PREFERENCES.toString());
                        editor.apply();
                        break;
                    case R.id.internal:
                        editor.putString(
                                getResources().getString(R.string.shared_pref_storage_type_key),
                                INTERNAL.toString());
                        editor.apply();
                        break;
                    case R.id.external:
                        editor.putString(
                                getResources().getString(R.string.shared_pref_storage_type_key),
                                EXTERNAL.toString());
                        editor.apply();
                        break;
                    case R.id.sql:
                        editor.putString(
                                getResources().getString(R.string.shared_pref_storage_type_key),
                                DATABASE.toString());
                        editor.apply();
                        break;
                    /*case R.id.memory:
                        editor.putString(
                                getResources().getString(R.string.shared_pref_storage_type_key),
                                MEMORY.toString());
                        editor.apply();
                        break;*/
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.nav_tasks:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_settings:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void loadStorageType(SharedPreferences sharedPreferences) {
        String storageType = sharedPreferences.getString(getResources().getString(R.string.shared_pref_storage_type_key), "");
        switch (StorageType.valueOf(storageType)) {
            case SHARED_PREFERENCES:
                ((RadioButton)findViewById(R.id.sharedPref)).setChecked(true);
                break;
            case INTERNAL:
                ((RadioButton)findViewById(R.id.internal)).setChecked(true);
                break;
            case EXTERNAL:
                ((RadioButton)findViewById(R.id.external)).setChecked(true);
                break;
            case DATABASE:
                ((RadioButton)findViewById(R.id.sql)).setChecked(true);
                break;
            case MEMORY:
                //((RadioButton)findViewById(R.id.memory)).setChecked(true);
                break;
        }
    }
}
