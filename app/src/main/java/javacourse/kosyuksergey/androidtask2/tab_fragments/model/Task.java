package javacourse.kosyuksergey.androidtask2.tab_fragments.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Task implements Parcelable{
    private String title;
    private String description;
    private String type = "add";

    public Task (){
        title = "";
        description = "";
        type = "add";
    }

    public Task(String title, String description) {
        this.title = title;
        this.description = description;
    }

    protected Task(Parcel in) {
        title = in.readString();
        description = in.readString();
        type = in.readString();
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(type);
    }

    public String getType() {
        return type;
    }

    public Task setType(String type) {
        this.type = type;
        return this;
    }
}
