package javacourse.kosyuksergey.androidtask2.tab_fragments;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import javacourse.kosyuksergey.androidtask2.R;
import javacourse.kosyuksergey.androidtask2.adapters.TaskListAdapter;
import javacourse.kosyuksergey.androidtask2.loaders.TaskLoader;
import javacourse.kosyuksergey.androidtask2.tab_fragments.model.Task;

public class Tab1Fragment extends Fragment {

    private ListView listView;
    private TaskListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab1_fragment, container, false);

        listView = view.findViewById(R.id.all_tasks);

        adapter = new TaskListAdapter(view.getContext(), R.layout.list_item_layout, new ArrayList<Task>());
        listView.setAdapter(adapter);

        getActivity().getSupportLoaderManager().initLoader(R.id.task_loader_id, null, loaderCallbacks);
        return view;
    }

    private LoaderManager.LoaderCallbacks<ArrayList<Task>> loaderCallbacks = new LoaderManager.LoaderCallbacks<ArrayList<Task>>() {
        @NonNull
        @Override
        public Loader<ArrayList<Task>> onCreateLoader(int id, @Nullable Bundle args) {
            return new TaskLoader(getContext());
        }

        @Override
        public void onLoadFinished(@NonNull Loader<ArrayList<Task>> loader, ArrayList<Task> data) {
            adapter.swapData(data);
        }

        @Override
        public void onLoaderReset(@NonNull Loader<ArrayList<Task>> loader) {
            adapter.swapData(new ArrayList<Task>());
        }
    };
 }
