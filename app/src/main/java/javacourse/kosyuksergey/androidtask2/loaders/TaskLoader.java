package javacourse.kosyuksergey.androidtask2.loaders;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.LocalBroadcastManager;

import java.util.ArrayList;

import javacourse.kosyuksergey.androidtask2.R;
import javacourse.kosyuksergey.androidtask2.data.StorageFactory;
import javacourse.kosyuksergey.androidtask2.data.StorageType;
import javacourse.kosyuksergey.androidtask2.data.storages.Storage;
import javacourse.kosyuksergey.androidtask2.tab_fragments.model.Task;

public class TaskLoader extends AsyncTaskLoader<ArrayList<Task>> {
    private static final String ACTION = "TASKFORCE";

    public TaskLoader(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getContext());
        IntentFilter filter = new IntentFilter(ACTION);
        manager.registerReceiver(broadcastReceiver, filter);


        forceLoad();
    }

    @Override
    public ArrayList<Task> loadInBackground() {
        StorageFactory storageFactory = new StorageFactory();
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(getContext());
        String storageType = sharedPreferences.getString(
                getContext().getResources().getString(R.string.shared_pref_storage_type_key),
                "");

        Storage storage = storageFactory.getStorage(StorageType.valueOf(storageType), getContext());
        return storage.getStorage();
    }

    @Override
    public void deliverResult(ArrayList<Task> data) {
        super.deliverResult(data);
    }

    @Override
    protected void onReset() {
        super.onReset();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            forceLoad();
        }
    };
}
