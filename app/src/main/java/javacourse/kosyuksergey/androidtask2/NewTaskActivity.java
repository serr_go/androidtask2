package javacourse.kosyuksergey.androidtask2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import javacourse.kosyuksergey.androidtask2.data.storages.Storage;
import javacourse.kosyuksergey.androidtask2.data.StorageFactory;
import javacourse.kosyuksergey.androidtask2.data.StorageType;
import javacourse.kosyuksergey.androidtask2.tab_fragments.model.Task;

public class NewTaskActivity extends AppCompatActivity {

    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        Task task = intent.getParcelableExtra("Edit Item");
        if (task == null) {
            task = new Task();
        }

        type = task.getType();
        EditText editTitle = findViewById(R.id.edit_title);
        editTitle.setText(task.getTitle());
        EditText editDescription = findViewById(R.id.edit_description);
        editDescription.setText(task.getDescription());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_task_menu, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save_task_item)
            saveTask();
        else
            onBackPressed();
        return true;
    }

    public void saveTask() {
        String title = ((EditText)findViewById(R.id.edit_title)).getText().toString();
        String description = ((EditText)findViewById(R.id.edit_description)).getText().toString();
        Task task = new Task(title, description);

        StorageFactory storageFactory = new StorageFactory();
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(this);
        String storageType = sharedPreferences.getString(
                this.getResources().getString(R.string.shared_pref_storage_type_key),
                "");

        Storage storage;
        switch (type) {
            case "edit" :
                storage = storageFactory.getFeatureStorage(StorageType.valueOf(storageType), this);
                break;
            case "add" :
                storage = storageFactory.getStorage(StorageType.valueOf(storageType), this);
                break;
            default:
                storage = null;
        }

        storage.addToStorage(task);
    }
}
